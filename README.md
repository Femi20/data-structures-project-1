### Objectives ###

	Refresh C/C++ programming skills. Use C++ I/O streams, string class, and STL containers and algorithms.
	Use makefile to organize and compile programs. 
	Use debugger to identify and address programming problems.

### Statement of Work ###
	
	Implement a program that collects the statistics of word, number, 
	and character usage in a file (redirected as the standard input). 
					
					
### How do you get set up? ###

	To create executable: Type 'make proj1.x'
	To run: Type 'proj1.x < test0'

