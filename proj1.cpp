#include <iostream>
#include <iomanip>
#include <string>
#include <iterator>
#include <algorithm>
#include <unordered_map>
#include <vector>
using namespace std;


typedef unordered_map<char, vector<int>> charmap;
typedef unordered_map<string, vector<int>> wordmap;
typedef unordered_map<string, vector<int>> numMap;

//Finds every unique character in the string
//and stores the character and its respective
//frequency in a map.Returns number of unique
//characters
//<Key:Character, Mapped_Value:frequency>
void findChars(charmap& map, string& s)
{
	 s += '\n';	
	auto itr = s.begin();
	while(itr != s.end())
	{
		auto found = map.find(*itr);
	
		if(found == map.end())
		{
			
		  map.insert(pair<char, vector<int> > (*itr,{1, int(*itr)}));

		}
		else 
			++(found -> second)[0];
		itr++;
	}

}	


//Finds every unique word in the string
//and stores the word and its respective
//frequency in a map.
//<Key:Word, Mapped_Value:frequency>
void findWords(wordmap& map, string& s,int& p)
{	
	string str1;
	string str(s);
	transform(str.begin(), str.end(), str.begin(), ::tolower);
	str += '|';
	auto itr = str.begin();


        while(itr != str.end())
        {
		if(isalpha(*itr)&& !isspace(*itr) && !isblank(*itr))
		{
			str1 += *itr;
			++itr;
			continue;
		}
                auto found = map.find(str1);

                if(found == map.end())
                {      
		 	map.insert(pair<string, vector<int> > (str1, {1,p++}));     	
                }
                else
                        ++(found -> second[0]);
		
		str1.clear();
		++itr;
        }

}
 
//Finds every unique number in the string
//and stores the number and its respective
//frequency in a map.
//<Key:Number, Mapped_Value:frequency>
void findNums(numMap& map, string& s, int& p)
{
	s += '|';
	 auto itr = s.begin();
	string str1;

        while(itr != s.end())	
        {
                if(isdigit(*itr) )
                {
                        str1 += *itr;
                        ++itr;
                        continue;
                }
                auto found = map.find(str1);

                if(found == map.end())
                {
			 map.insert(pair<string, vector<int> > (str1, {1,p++}));
                }
                else
                        ++(found -> second[0]);

                str1.clear();
                ++itr;
	}
}


bool wndescSort(const pair<string, vector<int>> &a, const pair<string, vector<int>> &b)
{	
	if(a.second[0] == b.second[0])
		return a.second[1] < b.second[1];
	
	return a.second[0] > b.second[0];
}      

bool cdescSort(const pair<char, vector<int>> &a, const pair<char, vector <int>> &b)
{
	if(a.second[0] == b.second[0])
		return a.second[1] < b.second[1];
	
	return a.second[0] > b.second[0];
}                      

int maxLength(const vector<pair<string, vector<int>>> &v, const vector<pair<string, vector<int>>> &v1)
{
	int max = 0;	
	
	if(v.size() >= 10)
	{	
		for(int i = 0; i < 10; i++)
		{
			if(max < v[i].first.length())
				max = v[i].first.length();
		}
	}
	else
	{
		for(int i = 0; i < v.size(); i++)
                {
                        if(max < v[i].first.length())
                                max = v[i].first.length();           
                }
	}
       

	 if(v1.size() >= 10)
        {
                for(int i = 0; i < 10; i++)
                {
                        if(max < v1[i].first.length())
                                max = v1[i].first.length();
                }
        }
        else
        {
                for(int i = 0; i < v1.size(); i++)
                {
                        if(max < v1[i].first.length())
                                max = v1[i].first.length();
                }
        }
	
		return max;

	
}
	                                                                                                                                                             
int main()
{
        charmap charCount;
	wordmap wordCount;
	numMap numCount;
	string s;	
        int pos = 0;
		

	while(getline(cin,s))
	{	
	 	findChars(charCount, s);
		findWords(wordCount, s, pos); 
		findNums(numCount,s, pos);
	}


	vector<pair<char, vector<int>>> c(charCount.begin(), charCount.end());
	 vector<pair<string, vector<int>>> w(wordCount.begin(), wordCount.end());
	 vector<pair<string, vector<int>>> n(numCount.begin(), numCount.end());
	
	sort(c.begin(), c.end(), cdescSort);
	sort(w.begin(), w.end(), wndescSort);
	sort(n.begin(), n.end(), wndescSort); 	
	
	w.erase(w.begin());
	n.erase(n.begin());
	if(charCount.size() < 10)
	{
		cout << "Total " << charCount.size() << " different characters, " << charCount.size() << " most used characters: \n";
		 for(unsigned int i = 0; i < charCount.size(); i++)
        {
		if(c[i].first == '\n')
		{
			cout << "No. " << i << ": ";
			cout << setw(maxLength(w,n) + 5) << left << "\\n" <<  c[i].second[0]<< "\n";
			continue;
		}

                cout << "No. " << i << ": ";
                cout << setw(maxLength(w,n) + 5) << left <<  c[i].first <<  c[i].second[0]<< "\n";
        }
		cout << "\n";
	}

	else
	{
		cout << "Total " << charCount.size() << " different characters, 10 most used characters: \n";
	for(int i = 0; i < 10; i++)	
	{
		    if(c[i].first == '\n')
                {
                        cout << "No. " << i << ": ";
                        cout << setw(maxLength(w,n) + 5) << left << "\\n" <<  c[i].second[0]<< "\n";
                        continue;
                }

		cout << "No. " << i << ": ";
		cout << setw(maxLength(w,n) + 5) << left <<  c[i].first <<  c[i].second[0]<< "\n";
	}
		   cout << "\n";
	}

	 if(w.size() < 10)
	{
                cout << "Total " << w.size() << " different words, " << w.size() << " most used words: \n";
		  for(unsigned int i = 0; i < w.size(); i++)
        {
                cout << "No. " << i <<  ": ";
                cout << setw(maxLength(w,n) + 5) << left << w[i].first <<  w[i].second[0] << "\n";
        }
		   cout << "\n";
        }
        else
	{
                cout << "Total " << w.size() << " different words, 10 most used words: \n";
        for(int i = 0; i < 10; i++)
        {
                cout << "No. " << i << ": ";
                cout <<  setw(maxLength(w,n) + 5) << left << w[i].first <<  w[i].second[0] << "\n";
        }
		   cout << "\n";
	}
	
	 if(n.size() < 10)
	{
                cout << "Total " << n.size() << " different numbers, " << n.size() << " most used numbers: \n";
		  for(unsigned int i = 0; i < n.size(); i++)
        {
                cout << "No. " << i << ": ";
                cout <<  setw(maxLength(w,n) + 5) << left <<  n[i].first <<  n[i].second[0] << "\n";
        }
	
	}

        
        else
	{
                cout << "Total " << n.size() << " different numbers, 10 most used numbers: \n";
        for(int i = 0; i < 10; i++)
        {
                cout <<  "No. " << i << ": ";
                cout <<  setw(maxLength(w,n) + 5) << left << n[i].first  << n[i].second[0] << "\n";
        }

	}

	return(EXIT_SUCCESS);
	

}
	

