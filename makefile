#CXX=g++47
CXXFLAGS=-std=c++11 -o

.SUFFIXES: .x

.PHONY: clean


clean:
	rm -f *.o *.x core.*

%.x : %.cpp
	$(CXX) $(CXXFLAGS) $@ $<
